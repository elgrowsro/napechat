$(document).ready(function(){
    $('.anchor').on('click', function(){
        elementClick = $(this).attr("href");
         destination = $(elementClick).offset().top;

           $('html, body').animate( { scrollTop: destination }, 1100 );



         return false;
    });
    //HM SLIDER
    var $liHmSlider = $('#hm-bxslider li'),
        $captionHmSlider = $('#hm-bxslider-caption');
        // $hrefHmSlider = $('#hm-bxslider-href');
    $('#hm-bxslider').bxSlider({
            prevSelector: '#hm-bxslider-left',
            nextSelector: '#hm-bxslider-right',
            prevText: '',
            nextText: '',
            auto: true,
            onSliderLoad: function(currentIndex) {

                $captionHmSlider.html($liHmSlider.eq(currentIndex ).data("caption"));
                // $hrefHmSlider.prop('href', $liHmSlider.eq(currentIndex ).data("href"));
            },
            onSlideBefore: function($slideElement, oldIndex, newIndex){
                $captionHmSlider.html($slideElement.data("caption"));
                // $hrefHmSlider.prop('href', $slideElement.data("href"));
            }
    });

    //WRAPPER TABLE
    $('.content table').wrap('<div class="wrap-tbl"></div>');


    //MAP COVER
    $('.map__cover').on('click', function(e){
        var $this = $(this);
        $this.addClass('hidden');
    });
    //MAP COVER
    $('body').on('click', function(e){
        
        $('.map__cover').removeClass('hidden');
    });
    $('.map').on('click', function(e){
        e.stopPropagation();
    })
});
